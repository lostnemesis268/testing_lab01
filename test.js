const assert = require('assert');
const math = require('./math');
describe ('file to be tested',()=>{
    context('function to be tested',()=>{
        it('should do something',()=>{
            assert.equal(1,1);
        });
        it('should do another thing', ()=>{
            assert.deepEqual([1,2,3],[1,2,3]);
            assert.deepEqual({name: 'mark'},{name: 'mark'});
        });
    });
});

describe('file math', ()=>{
    context('funtion add1', ()=>{
        it('shold add(1,2)',()=>{
            assert.equal(math.add1(1,2),3);
        });
        
    });
    context('function add2',()=>{
        it('should add2(5,5)',()=>{
            assert.equal(math.add2(5,5),10);
        });
    });
});