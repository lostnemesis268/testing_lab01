const chai = require('chai');
const expect = chai.expect;
const math = require('./math');


describe('Test chai',()=>{
    it('should compare thing by expect',()=>{
        expect(1).to.equal(1);
    });
    it('should compare anoter things by expect',()=>{
        expect(5>8).to.be.false;
        expect({name: 'mark'}).to.deep.equal({name: 'mark'});
        expect({name: 'mark'}).to.have.property('name').to.equal('mark');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('mark').to.be.a('string');
        expect('mark'.length).to.equal(4);
        expect('mark').to.lengthOf(4);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('Math module',()=>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('add(1,1)ควรส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equal(2);
        });
    });
});